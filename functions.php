<?php
	/*-----------------------------------------------------------------------------------*/
	/* This file will be referenced every time a template/page loads on your Wordpress site
	/* This is the place to define custom fxns and specialty code
	/*-----------------------------------------------------------------------------------*/

// Define the version so we can easily replace it throughout the theme
define( 'NAKED_VERSION', 1.0 );

/*-----------------------------------------------------------------------------------*/
/*  Set the maximum allowed width for any content in the theme
/*-----------------------------------------------------------------------------------*/
if ( ! isset( $content_width ) ) $content_width = 900;

/*-----------------------------------------------------------------------------------*/
/* Add Rss feed support to Head section
/*-----------------------------------------------------------------------------------*/
add_theme_support( 'automatic-feed-links' );


/*-----------------------------------------------------------------------------------*/
/* Register main menu for Wordpress use
/*-----------------------------------------------------------------------------------*/
register_nav_menus(
	array(
		'primary'	=>	__( 'Primary Menu', 'naked' ), // Register the Primary menu
		// Copy and paste the line above right here if you want to make another menu,
		// just change the 'primary' to another name
	)
);

/*-----------------------------------------------------------------------------------*/
/* Activate sidebar for Wordpress use
/*-----------------------------------------------------------------------------------*/
function naked_register_sidebars() {
	register_sidebar(array(				// Start a series of sidebars to register
		'id' => 'sidebar', 					// Make an ID
		'name' => 'Sidebar',				// Name it
		'description' => 'Take it on the side...', // Dumb description for the admin side
		'before_widget' => '<div>',	// What to display before each widget
		'after_widget' => '</div>',	// What to display following each widget
		'before_title' => '<h3 class="side-title">',	// What to display before each widget's title
		'after_title' => '</h3>',		// What to display following each widget's title
		'empty_title'=> '',					// What to display in the case of no title defined for a widget
		// Copy and paste the lines above right here if you want to make another sidebar,
		// just change the values of id and name to another word/name
	));
}
// adding sidebars to Wordpress (these are created in functions.php)
add_action( 'widgets_init', 'naked_register_sidebars' );

/*-----------------------------------------------------------------------------------*/
/* Enqueue Styles and Scripts
/*-----------------------------------------------------------------------------------*/

function naked_scripts()  {

	// get the theme directory style.css and link to it in the header
	wp_enqueue_style('style.css', get_stylesheet_directory_uri() . '/style.css');

	// add fitvid
	wp_enqueue_script( 'naked-fitvid', get_template_directory_uri() . '/js/jquery.fitvids.js', array( 'jquery' ), NAKED_VERSION, true );

	// add theme scripts
	wp_enqueue_script( 'naked', get_template_directory_uri() . '/js/theme.min.js', array(), NAKED_VERSION, true );

}
add_action( 'wp_enqueue_scripts', 'naked_scripts' ); // Register this fxn and allow Wordpress to call it automatcally in the header

add_theme_support('post-thumbnails');



// カスタム投稿タイプを作成する
add_action('init', 'add_articles_post_type');
function add_articles_post_type() {
    $params = array(
            'labels' => array(
                    'name' => '新着記事',
                    'singular_name' => '新着記事',
                    'add_new' => '新規追加',
                    'add_new_item' => '新着記事を新規追加',
                    'edit_item' => '新着記事を編集する',
                    'new_item' => '新規新着記事',
                    'all_items' => '新着記事一覧',
                    'view_item' => '新着記事の説明を見る',
                    'search_items' => '検索する',
                    'not_found' => '新着記事が見つかりませんでした。',
                    'not_found_in_trash' => 'ゴミ箱内に新着記事が見つかりませんでした。'
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array(
                    'title',
                    'editor',
                    'author',
                    'custom-fields',
            ),
						'taxonomies' => array('articles_category','articles_tag')
		);
		register_post_type('articles', $params);
}

// カスタム投稿タイプ（websites）用のカテゴリ＆タグを作成する
add_action('init', 'create_articles_taxonomies');
function create_articles_taxonomies() {
    // カテゴリを作成
    $labels = array(
            'name'                => '「新着記事」カテゴリ',        //複数系のときのカテゴリ名
            'singular_name'       => '「新着記事」カテゴリ',        //単数系のときのカテゴリ名
            'search_items'        => '「新着記事」カテゴリを検索',
            'all_items'           => '全ての「新着記事」カテゴリ',
            'parent_item'         => '親カテゴリ',
            'parent_item_colon'   => '親カテゴリ:',
            'edit_item'           => '「新着記事」カテゴリを編集',
            'update_item'         => '「新着記事」カテゴリを更新',
            'add_new_item'        => '新規「新着記事」カテゴリを追加',
            'new_item_name'       => '新規「新着記事」カテゴリ',
            'menu_name'           => '「新着記事」カテゴリ'        //ダッシュボードのサイドバーメニュー名
    );
    $args = array(
            'hierarchical'        => true,
            'labels'              => $labels,
            'rewrite'             => array( 'slug' => 'articles_cat' )
    );
    register_taxonomy( 'articles_category', 'articles', $args );

    // タグを作成
    $labels = array(
            'name'                => '「新着記事」タグ',        //複数系のときのタグ名
            'singular_name'       => '「新着記事」タグ',        //単数系のときのタグ名
            'search_items'        => '「新着記事」タグを検索',
            'all_items'           => '全ての「新着記事」タグ',
            'parent_item'         => null,
            'parent_item_colon'   => null,
            'edit_item'           => '「新着記事」タグを編集',
            'update_item'         => '「新着記事」タグを更新',
            'add_new_item'        => '新規「新着記事」タグを追加',
            'new_item_name'       => '新規「新着記事」タグ',
            'separate_items_with_commas'   => '「新着記事」タグをコンマで区切る',
            'add_or_remove_items'          => '「新着記事」タグを追加or削除する',
            'choose_from_most_used'        => 'よく使われている「新着記事」タグから選択',
            'not_found'                    => 'アイテムは見つかりませんでした',
            'menu_name'                    => '「新着記事」タグ'        //ダッシュボードのサイドバーメニュー名
    );
    $args = array(
            'hierarchical'            => false,
            'labels'                  => $labels,
            'update_count_callback'   => '_update_post_term_count',    //タグの動作に必要なCallback設定
            'rewrite'                 => array( 'slug' => 'articles_tag' )
    );

    register_taxonomy( 'articles_tag', 'articles', $args );
}
