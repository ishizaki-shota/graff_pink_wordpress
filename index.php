<?php
/**
 * The template for displaying the home/index page.
 * This template will also be called in any case where the Wordpress engine
 * doesn't know which template to use (e.g. 404 error)
 */

get_header(); // This fxn gets the header.php file and renders it ?>


<section id="headimg">
	<div id="headerimg"><img src="<?php bloginfo('template_directory'); ?>/images/header.png"></div>
	<div id="headertitle">
		<h1>WELCOME TO PINK!</h1>
		<h2>A multi purpose theme from cssauthor.com</h2>
	</div>
</section>

<div id="wrapper">
	<div id="main">
		<section id="maintitle">
			<h1>Lorem Ipsum is simply dummy!!</h1>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the indus-<br>try's standard dummy text ever since the 1500s, when an unknown</p>
		</section>
		<section id="icons">
			<div class="ticket"><a href="<?php echo get_term_link(6, 'articles_category'); ?>"><img class="ura" src="<?php bloginfo('template_directory'); ?>/images/tickets_hover.png"><img class="omote" src="<?php bloginfo('template_directory'); ?>/images/tickets.png"></a></div>
			<div class="camera"><a href="<?php echo get_term_link(7, 'articles_category'); ?>"><img class="ura" src="<?php bloginfo('template_directory'); ?>/images/camera_hover.png"><img class="omote" src="<?php bloginfo('template_directory'); ?>/images/camera.png"></a></div>
			<div class="beer"><a href="<?php echo get_term_link(8, 'articles_category'); ?>"><img class="ura" src="<?php bloginfo('template_directory'); ?>/images/beer_hover.png"><img class="omote" src="<?php bloginfo('template_directory'); ?>/images/beer.png"></a></div>
			<div class="devices"><a href="<?php echo get_term_link(9, 'articles_category'); ?>"><img class="ura" src="<?php bloginfo('template_directory'); ?>/images/devices_hover.png"><img class="omote" src="<?php bloginfo('template_directory'); ?>/images/devices.png"></a></div>
		</section>
	</div>


	<section id="news">
			<?php if ( have_posts() ) :
			// Do we have any posts in the databse that match our query?
			// In the case of the home page, this will call for the most recent posts
			?>
				<?php while ( have_posts() ) : the_post();
				// If we have some posts to show, start a loop that will display each one the same way
				?>
					<article class="post">
						<?php if (has_post_thumbnail()) : ?>
				        <?php the_post_thumbnail('thumbnail'); ?>
				    <?php else : ?>
				        <img src="<?php bloginfo('template_url'); ?>/images/noimage.png" class="wp-post-image" alt="デフォルト画像" />
				    <?php endif ; ?>

						<h1 class="title">
							<a href="<?php the_permalink(); // Get the link to this post ?>" title="<?php the_title(); ?>">
								<?php the_title(); // Show the title of the posts as a link ?>
							</a>
						</h1>

						<div class="the-content">
							<?php the_content( 'Continue...' );
							// This call the main content of the post, the stuff in the main text box while composing.
							// This will wrap everything in p tags and show a link as 'Continue...' where/if the
							// author inserted a <!-- more --> link in the post body
							?>

							<?php wp_link_pages(); // This will display pagination links, if applicable to the post ?>
						</div><!-- the-content -->

					</article>

				<?php endwhile; // OK, let's stop the posts loop once we've exhausted our query/number of posts ?>

				<!-- pagintation -->
				<div id="pagination" class="clearfix">
					<div class="past-page"><?php previous_posts_link( 'newer' ); // Display a link to  newer posts, if there are any, with the text 'newer' ?></div>
					<div class="next-page"><?php next_posts_link( 'older' ); // Display a link to  older posts, if there are any, with the text 'older' ?></div>
				</div><!-- pagination -->


			<?php else : // Well, if there are no posts to display and loop through, let's apologize to the reader (also your 404 error) ?>

				<article class="post error">
					<h1 class="404">Nothing has been posted like that yet</h1>
				</article>
	</section>

			<?php endif; // OK, I think that takes care of both scenarios (having posts or not having any posts) ?>
</div><!-- #wrapper -->
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>
