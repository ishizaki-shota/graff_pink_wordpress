<?php
	/*-----------------------------------------------------------------------------------*/
	/* This template will be called by all other template files to finish
	/* rendering the page and display the footer area/content
	/*-----------------------------------------------------------------------------------*/
?>

</main><!-- / end page container, begun in the header -->

<footer>
	<div id="footer-wrap">
		<nav id="footer">
			<a href="<?php echo get_post_type_archive_link('articles') ?>"><h1>新着記事一覧</h1></a>
			<div class="footerbox">
				<div class="menuwrap">
					<a href="<?php echo get_term_link(6, 'articles_category'); ?>"><h2>Tickets</h2></a>
					<ul>
						<?php
						query_posts( array('post_type' => 'articles','taxonomy' => 'articles_category','term' => 'tickets','posts_per_page' => 5) );
						while(have_posts()) {
				        the_post();
				        $title = get_the_title();
				        $link = get_post_meta($post->ID, "url", true);
				        echo '<li><a href="' . $link . '">' . $title . "</a></li>";
				    }
						wp_reset_query();
						?>
					</ul>


				</div>
				<div class="menuwrap">
					<a href="<?php echo get_term_link(7, 'articles_category'); ?>"><h2>Camera</h2></a>
					<ul>
						<?php
						query_posts( array('post_type' => 'articles','taxonomy' => 'articles_category','term' => 'camera','posts_per_page' => 5) );
						while(have_posts()) {
				        the_post();
				        $title = get_the_title();
				        $link = get_post_meta($post->ID, "url", true);
				        echo '<li><a href="' . $link . '">' . $title . "</a></li>";
				    }
						wp_reset_query();
						?>

					</ul>
				</div>
			</div>
			<div class="footerbox">
				<div class="menuwrap">
					<a href="<?php echo get_term_link(8, 'articles_category'); ?>"><h2>Beer</h2></a>
					<ul>
						<?php
						query_posts( array('post_type' => 'articles','taxonomy' => 'articles_category','term' => 'beer','posts_per_page' => 5) );
						while(have_posts()) {
				        the_post();
				        $title = get_the_title();
				        $link = get_post_meta($post->ID, "url", true);
				        echo '<li><a href="' . $link . '">' . $title . "</a></li>";
				    }
						wp_reset_query();
						?>

					</ul>
				</div>
				<div class="menuwrap">
					<a href="<?php echo get_term_link(9, 'articles_category'); ?>"><h2>Devices</h2></a>
					<ul>
						<?php
						query_posts( array('post_type' => 'articles','taxonomy' => 'articles_category','term' => 'devices','posts_per_page' => 5) );
						while(have_posts()) {
				        the_post();
				        $title = get_the_title();
				        $link = get_post_meta($post->ID, "url", true);
				        echo '<li><a href="' . $link . '">' . $title . "</a></li>";
				    }
						wp_reset_query();
						?>

					</ul>
				</div>
			</div>
		</nav>
		<div id="share">
			<p><img src="<?php bloginfo('template_directory'); ?>/images/facebook.png"><img src="<?php bloginfo('template_directory'); ?>/images/twitter.png"><img src="<?php bloginfo('template_directory'); ?>/images/www.png"><img src="<?php bloginfo('template_directory'); ?>/images/share.png"></p>
			<p class="copyright">Copyright © 2009–2013 Cssauthor.com</p>
		</div>
	</div>
</footer>

	</div><!-- .site-info -->
</footer><!-- #colophon .site-footer -->

<?php wp_footer();
// This fxn allows plugins to insert themselves/scripts/css/files (right here) into the footer of your website.
// Removing this fxn call will disable all kinds of plugins.
// Move it if you like, but keep it around.
?>
</div>

</body>
</html>
