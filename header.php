<?php
	/*-----------------------------------------------------------------------------------*/
	/* This template will be called by all other template files to begin
	/* rendering the page and display the header/nav
	/*-----------------------------------------------------------------------------------*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="description" content="テストページ.">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no">
<title>
	<?php bloginfo('name'); // show the blog name, from settings ?> |
	<?php is_front_page() ? bloginfo('description') : wp_title(''); // if we're on the home page, show the description, from the site's settings - otherwise, show the title of the post or page ?>
</title>
<!-- ogp-->
<meta property="og:type" content="website">
<meta property="og:title" content="kayac-html5-starter">
<meta property="og:site_name" content="kayac-html5-starter">
<meta property="og:description" content="テストページ.">
<meta property="og:locale" content="ja_JP">
<!-- twitter card-->
<meta property="twitter:card" content="summary">
<meta property="twitter:title" content="kayac-html5-starter">
<meta property="twitter:description" content="テストページ.">

<script src="js/script.js"></script>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // We are loading our theme directory style.css by queuing scripts in our functions.php file,
	// so if you want to load other stylesheets,
	// I would load them with an @import call in your style.css
?>

<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php wp_head();
// This fxn allows plugins, and Wordpress itself, to insert themselves/scripts/css/files
// (right here) into the head of your website.
// Removing this fxn call will disable all kinds of plugins and Wordpress default insertions.
// Move it if you like, but I would keep it around.
?>

</head>

<body
	<?php body_class();
	// This will display a class specific to whatever is being loaded by Wordpress
	// i.e. on a home page, it will return [class="home"]
	// on a single post, it will return [class="single postid-{ID}"]
	// and the list goes on. Look it up if you want more.
	?>
>
<div class="wrapper">

<header>
	<div id="overmenuwrap">
		<div id="menuwrap">
			<div id="logoimg"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?php bloginfo('template_directory'); ?>/images/Graffpink.png"></a></div>
			<div id="login"><a href="#"><img class="login" src="<?php bloginfo('template_directory'); ?>/images/Login.png">Login</a></div>
				<ul>
					<?php
					wp_nav_menu(array('theme_location' => 'main-menu'));
					?>
			 </ul>
	 </div>
 </div>
</header>

<!-- #masthead .site-header -->

<main class="main-fluid"><!-- start the page containter -->
